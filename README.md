Sainsburys online shopping receipt into CSV.

Turns a sainsbury's online shopping receipt into CSV, one line per item, with the format {item},{price}

Instructions:

1. Clone:

```
mkdir sainos
git clone https://gitlab.com/alexhoulton/sainos.git
cd sainos
```

2. Build the docker image:

```
docker build -t alex/sainos .
```

3. Run the script:

- Name your receipt `receipt.pdf` and then:

```
docker run -v /path/to/receipt:/usr/src/app/files --rm alex/sainos
```
