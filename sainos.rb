#!/usr/bin/env ruby
require 'pdf-reader'
reader = PDF::Reader.new('/usr/src/app/files/receipt.pdf')

lines = []
items = []
pages_lines = []
index = 0
reader.pages.each do |page|
  lines = page.text.split(/^\n/)
  if index == 0
    until lines.first.include?("Delivery summary") do
      lines.shift
    end
  end
  pages_lines[index] = lines
  index += 1
end

pages_lines.each { |p|
  lines.push(p.select { |l| l.include?("£") })
}
lines.flatten!
lines.map! { |l| l.count("£") == 2 ? l.split("\n") : l }.flatten!
lines.each { |i|
  matches = i.match(/^ {0,2}([0-9a-z.]+) ([a-zA-z0-9 '.,()&?ﬀ\-\n]+)(£[0-9.]*)/)
  next unless matches
  qty, name, price = matches.captures
  name.gsub!(/,/, "")
  name.gsub!(/\R+/, "")
  name.gsub!(/ +/, " ")
  item = {name: name.strip(), qty: qty, price: price}
  items.push(item)
}
items.each { |k| puts "#{k[:name]},#{k[:price]}" }
